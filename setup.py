# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for CATDS SMOS Data Center datasets.
'''

requires = [
   # 'cerbere>=2.0.0',
]

setup(
    name='cerberecontrib-catds',
    version='1.0',
    url='',
    license='GPLv3',
    authors='Jean-François Piollé',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for CATDS products',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'cerbere.plugins': [
            'CATDSCLF31ADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCLF31ADataset',
            'CATDSCLF33ADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCLF33ADataset',
            'CATDSCLF3EADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCLF3EADataset',
            'CATDSCLF3MADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCLF3MADataset',
            'CATDSCSF2QADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCSF2QADataset',
            'CATDSCSQ3ADataset = cerberecontrib_catds.dataset.catdsncdataset:CATDSCSQ3ADataset',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
)
