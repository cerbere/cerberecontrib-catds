=====================================
Cerbere extensions for CATDS datasets
=====================================

Classes derived from Cerbere base class :class:`~cerbere.dataset.dataset.Dataset` 
to read CATDS files.

There is a class provided for type of CATDS products as their format is not
consistent from one product to another.

Installation
============

Requires ``cerbere>=2.0.0``

Test data
=========

Sample products for each class can be found at:

ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/

They are used by the unitary tests in ``tests`` directory.


Usage example
=============

.. code-block:: python

  import cerbere

  # open file
  dst = cerbere.open_dataset(
      '/home/jfpiolle/git/cerbere/tests/data/CATDSCSQ3ADataset/SM_OPER_MIR_CSQ3A__20200425T000000_20200504T235959_317_001_7.DBL.nc',
      'CATDSCSQ3ADataset'
  )

  print(dst.get_times())
  print(dst.get_lon())
  print(dst.fieldnames)
  print(dst.get_values('Mean_Sea_Surface_Salinity'))








