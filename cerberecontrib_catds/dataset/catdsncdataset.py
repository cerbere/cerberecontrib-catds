"""
"""
from pathlib import Path

from dateutil import parser
import numpy as np
import xarray as xr

from cerbere.dataset.ncdataset import NCDataset


class CATDSNCDataset(NCDataset):

    def _open_dataset(self, *args, **kwargs) -> 'xr.Dataset':
        """
        """
        ds = super(CATDSNCDataset, self)._open_dataset(*args, **kwargs)
        ds.coords['time'] = xr.DataArray(
            np.datetime64('2000-01-01T00:00:00') +
            ds['Mean_Acq_Time_Days'].values.astype('timedelta64[D]') +
            ds['Mean_Acq_Time_Seconds'].values.astype('timedelta64[s]'),
            dims=['lat', 'lon']
        )
        ds = ds.drop([
            'Mean_Acq_Time_Days', 'Mean_Acq_Time_Seconds']
        )
        ds.attrs['time_coverage_start'] = parser.parse(
            Path(self.url).name.split('_')[-5])
        ds.attrs['time_coverage_end'] = parser.parse(
            Path(self.url).name.split('_')[-4])
        return ds


class CATDSMonthlyNCDataset(NCDataset):

    def _open_dataset(self, *args, **kwargs) -> 'xr.Dataset':
        """
        """
        ds = super(CATDSMonthlyNCDataset, self)._open_dataset(*args, **kwargs)
        ds.coords['time'] = xr.DataArray(
            [np.datetime64(ds.attrs['abs_day_start'].replace('UTC=', '')) +
             np.timedelta64(15, 'D')],
            dims=['time']
        )
        ds.attrs['time_coverage_start'] = parser.parse(
            Path(self.url).name.split('_')[-5])
        ds.attrs['time_coverage_end'] = parser.parse(
            Path(self.url).name.split('_')[-4])
        return ds


class CATDSNoDateNCDataset(NCDataset):

    def _open_dataset(self, delta, *args, **kwargs) -> 'xr.Dataset':
        """
        """
        ds = super(CATDSNoDateNCDataset, self)._open_dataset(*args, **kwargs)
        date = parser.parse(Path(self.url).name.split('_')[-5])
        ds.coords['time'] = xr.DataArray(
            [np.datetime64(date) + delta],
            dims=['time']
        )
        ds.attrs['time_coverage_start'] = parser.parse(
            Path(self.url).name.split('_')[-5])
        ds.attrs['time_coverage_end'] = parser.parse(
            Path(self.url).name.split('_')[-4])
        return ds


class CATDSCLF31ADataset(CATDSNCDataset):
    pass


class CATDSCLF33ADataset(CATDSNCDataset):
    pass


class CATDSCLF3EADataset(CATDSNCDataset):
    pass


class CATDSCLF3MADataset(CATDSMonthlyNCDataset):
    pass


class CATDSCSF2QADataset(CATDSNoDateNCDataset):
    def _open_dataset(self, *args, **kwargs):
        return super(CATDSCSF2QADataset, self)._open_dataset(
            np.timedelta64(12, 'h'), *args, **kwargs)


class CATDSCSQ3ADataset(CATDSNoDateNCDataset):
    def _open_dataset(self, *args, **kwargs):
        return super(CATDSCSQ3ADataset, self)._open_dataset(
            np.timedelta64(int(4.5 * 24 * 60), 'm'), *args, **kwargs)


class CATDSCSQ3AMDataset(CATDSNoDateNCDataset):
    def _open_dataset(self, *args, **kwargs):
        return super(CATDSCSQ3AMDataset, self)._open_dataset(
            np.timedelta64(15, 'D'), *args, **kwargs)
