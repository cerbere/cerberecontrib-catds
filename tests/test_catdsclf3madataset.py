"""

Test class for cerbere CATDS classes

:copyright: Copyright 2017 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class CATDSCLF3MADatasetChecker(Checker, unittest.TestCase):
    """Test class for CATDS files"""

    def __init__(self, methodName="runTest"):
        super(CATDSCLF3MADatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'CATDSCLF3MADataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'CylindricalGrid'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "SM_OPER_MIR_CLF3MA_20200501T000000_20200531T235959_300_001_7.DBL.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
